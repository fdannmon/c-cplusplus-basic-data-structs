#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void initStack(Node**);
void push(Node**, int);
int pop(Node**);
int length(Node**);
void showStack(Node**);

void initStack(Node **header)
{
  *header = NULL;
}

void push(Node **header, int number)
{
  Node *q;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->next = NULL;
  if(*header == NULL)
  {
    *header = q;
  } else {
    q->next = *header;
    *header = q;
  }
}

int pop(Node **header)
{
  int value = 0;
  if(*header == NULL)
  {
    perror("Stack is empty.\n");
  } else {
    value = (*header)->data;
    *header = (*header)->next;
  }
  return value;
}

int length(Node **header)
{
  Node *temp; temp = *header;
  if(temp == NULL)
  {
    return 0;
  } else {
    int quantity = 0;
    while(temp != NULL)
    {
      quantity++;
      temp = temp->next;
    }
    return quantity;
  }
}

void showStack(Node **header)
{
  Node *temp; temp = *header;
  if(temp == NULL)
  {
    printf("Stack is empty.\n");
  } else {
    while(temp != NULL)
    {
      printf("%d ", temp->data);
      temp = temp->next;
    }
    printf("\n");
  }
}


