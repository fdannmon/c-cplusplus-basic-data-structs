/*
 * Compiling in Linux with `gcc -std=11 main.c -o <filename>`
 */

#include "stack.h"

int main()
{
  Node *h1;

  //Init stack
  initStack(&h1);
  
  //Adding even numbers
  for(int i=0; i<18; i+=2)
  {
    push(&h1, i);
  }

  //Showing initial stack
  showStack(&h1);
  printf("Initial length is %d\n", length(&h1));

  //Popping elements in stack
  printf("Popping stack... %d\n", pop(&h1));
  showStack(&h1);
  printf("Length after pop is %d\n", length(&h1));

  //Second pop
  printf("Popping stack... %d\n", pop(&h1));
  showStack(&h1);
  printf("Length after pop is %d\n", length(&h1));

  //system("pause"); uncomment for Windows
  return 0;
}


