#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void createList(Node**);
void insertAtBeginning(Node**, int);
void insertAtEnd(Node**, int);
int length(Node**);
void deleteByIndex(Node**, int);
void showList(Node**);

void createList(Node **header)
{
  *header = NULL;
}

void insertAtStart(Node **header, int number)
{
  Node *q;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->next = NULL;

  if(*header == NULL)
  {
    *header = q;
  } else {
    q->next = *header;
    *header = q;
  }
}

void insertAtEnd(Node **header, int number)
{
  Node *q, *temp; temp = *header;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->next = NULL;

  if(*header == NULL)
  {
    *header = q;
  } else {
    while(temp->next != NULL)
    {
      temp = temp->next;
    }
    temp->next = q;
  }
}

int length(Node **header)
{
  Node *temp; temp = *header;

  if(*header == NULL)
  {
    return 0;
  } else {
    int quantity = 0;
    while(temp != NULL)
    {
      quantity++;
      temp = temp->next;
    }
    return quantity;
  }
}

void deleteByIndex(Node **header, int index)
{
  if(index < 0 || index >= length(header))
  {
    printf("Index is not correct.\n");
  } else if(length(header) == 1 && index == 0)
  {
    *header = NULL;
  } else {
    Node *temp, *befTemp;
    temp = *header;
    int delIndex = 0;
    while(temp != NULL)
    {
      if(index == delIndex)
      {
        printf("Index %d was deleted.\n", index);
        befTemp->next = temp->next;
        free(temp);
        break;
      }
      befTemp = temp;
      temp = temp->next;
      delIndex++;
    }
  }
}

void showList(Node **header)
{
  Node *temp; temp = *header;
  if(temp == NULL)
  {
    printf("List is empty.\n");
  } else {
    while(temp != NULL)
    {
      printf("%d ", temp->data);
      temp = temp->next;
    }
    printf("\n");
  }
}
