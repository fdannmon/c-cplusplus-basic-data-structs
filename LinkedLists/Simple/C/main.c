#include "list.h"

int main()
{
  Node *head1, *head2, *head3;
  createList(&head1);
  insertAtStart(&head1, 1);
  insertAtStart(&head1, 2);
  insertAtStart(&head1, 3);
  insertAtStart(&head1, 4);
  insertAtStart(&head1, 5);
  printf("List 1 is... ");
  showList(&head1);
  printf("Length of list 1 is %d\n", length(&head1));
  deleteByIndex(&head1, 2);
  showList(&head1);

  createList(&head2);
  insertAtEnd(&head2, 1);
  insertAtEnd(&head2, 2);
  insertAtEnd(&head2, 3);
  printf("List 2 is... ");
  showList(&head2);
  printf("Length of list 2 is %d\n", length(&head2));

  createList(&head3);
  printf("Length of list 3 is %d\n", length(&head3));
  return 0;
}
