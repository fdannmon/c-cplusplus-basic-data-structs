/*
 * Compiling in Linux with `gcc -std=11 main.c -o <filename>`
 */

#include "queue.h"

int main()
{
  Node *h1;

  //Init queue
  initQueue(&h1);
  
  //Adding odds 
  for(int i=1; i<19; i+=2)
  {
    push(&h1, i);
  }

  //Showing initial queue
  showQueue(&h1);
  printf("Initial length is %d\n", length(&h1));

  //Popping elements in queue
  printf("Popping queue... %d\n", pop(&h1));
  showQueue(&h1);
  printf("Length after pop is %d\n", length(&h1));

  //Second pop
  printf("Popping queue... %d\n", pop(&h1));
  showQueue(&h1);
  printf("Length after pop is %d\n", length(&h1));

  //system("pause"); uncomment for Windows
  return 0;
}
