#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void initQueue(Node**);
void push(Node**, int);
int pop(Node**);
int length(Node**);
void showQueue(Node**);

void initQueue(Node **header)
{
  *header = NULL;
}

void push(Node **header, int number)
{
  Node *q, *temp; temp = *header;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->next = NULL;
  if(temp == NULL)
  {
    *header = q;
  } else {
    while(temp->next != NULL)
    {
      temp = temp->next;
    }
    temp->next = q;
  }
}

int pop(Node **header)
{
  int value = 0;
  if(*header == NULL)
  {
    perror("Queue is empty.\n");
  } else {
    value = (*header)->data;
    *header = (*header)->next;
  }
  return value;
}

int length(Node **header)
{
  Node *temp; temp = *header;
  if(temp == NULL)
  {
    return 0;
  } else {
    int quantity = 0;
    while(temp != NULL)
    {
      quantity++;
      temp = temp->next;
    }
    return quantity;
  }
}

void showQueue(Node **header)
{
  Node *temp; temp = *header;
  if(temp == NULL)
  {
    printf("Queue is empty.\n");
  } else {
    while(temp != NULL)
    {
      printf("%d ", temp->data);
      temp = temp->next;
    }
    printf("\n");
  }
}


